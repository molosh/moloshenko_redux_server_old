<?php

use yii\db\Migration;

/**
 * Class m180606_083851_update_article_table
 */
class m180606_083851_update_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('article', 'tags', $this->string()->after('title'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180606_083851_update_article_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180606_083851_update_article_table cannot be reverted.\n";

        return false;
    }
    */
}
