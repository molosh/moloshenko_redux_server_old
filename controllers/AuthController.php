<?php

namespace app\controllers;

use app\models\ImageUpload;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\UploadedFile;

class AuthController extends Controller
{

    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        header('Access-Control-Allow-Origin: *');

        $user = [
            'LoginForm' => [
                'email' => Yii::$app->request->post('email'),
                'password' => Yii::$app->request->post('password'),
                'rememberMe' => 1
            ],
            'login-button' => ''
        ];

        if (!$user['LoginForm']['email'] || !$user['LoginForm']['password'])
            return 'SOME_FIELD_IS_EMPTY';

        if (!filter_var($user['LoginForm']['email'], FILTER_VALIDATE_EMAIL)) {
            return "INCORRECT_EMAIL_FORMAT";
        }


        $model = new LoginForm();
        if ($model->load($user) && $model->login()) {
            $userModel = new User();
            $currentUser = $userModel->findByEmail(Yii::$app->request->post('email'));

            $data = ArrayHelper::toArray($currentUser, [
                    'app\models\User' => [
                        'id',
                        'name',
                        'email',
                        'photo',
                    ]
                ]
            );

            return json_encode($data);
        } else {
            return 'INVALID_PAIR';
        }
    }

    /**
     * Logout action.
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionSignup()
    {
        header('Access-Control-Allow-Origin: *');
        $model = new SignupForm();

        $user = [
            'SignupForm' => [
                'name' => Yii::$app->request->post('name'),
                'email' => Yii::$app->request->post('email'),
                'password' => Yii::$app->request->post('password'),
                'confirm_password' => Yii::$app->request->post('confirm_password')
            ],
            'login-button' => ''
        ];

        if (!$user['SignupForm']['name'] ||
            !$user['SignupForm']['email'] ||
            !$user['SignupForm']['password'] ||
            !$user['SignupForm']['confirm_password'])
            return json_encode('SOME_FIELD_IS_EMPTY');

        if (User::findByUsername($user['SignupForm']['name']))
            return json_encode('NAME_IS_NOT_UNIQUE');

        if (User::findByEmail($user['SignupForm']['email']))
            return json_encode('EMAIL_IS_NOT_UNIQUE');

        if (!filter_var($user['SignupForm']['email'], FILTER_VALIDATE_EMAIL)) {
            return "INCORRECT_EMAIL_FORMAT";
        }

        if (strlen($user['SignupForm']['password']) < 6)
            return json_encode('PASSWORD_IS_SHORT');

        if ($user['SignupForm']['password'] !== $user['SignupForm']['confirm_password'])
            return json_encode('PASSWORDS_DO_NOT_MATCH');



        $model->load($user);

        if($model->signup())
        {
            $email = $model->email;
            $this->signupEmail($email);
            return json_encode('OK');
        }

        return json_encode('Error');
    }

    public function generateToken()
    {
        return bin2hex(random_bytes(78));
    }

    public function signupEmail($email)
    {
        $token = $this->generateToken();
        $user = User::find()->where(['email'=>$email])->one();
        $user->email_confirm_token = $token;
        $user->save();
        $this->testMailer($token, $email);
        Yii::$app->session->setFlash('alerts', "We have sent you a confirmation email on $email. Please, " .
            "confirm it and then login");

    }

    public function actionUpdate()
    {
        header('Access-Control-Allow-Origin: *');

        $inputName = Yii::$app->request->post('name');
        $inputEmail = Yii::$app->request->post('email');
        $file = UploadedFile::getInstanceByName( 'photo');
        $user = User::findIdentity(Yii::$app->request->post('user_id'));

        if (!$inputName || !$inputEmail)
            return 'SOME_FIELD_IS_EMPTY';


        if (User::findOthersByUsername($inputName, Yii::$app->request->post('user_id')))
            return 'NAME_IS_NOT_UNIQUE';

        if (User::findOthersByEmail($inputEmail, Yii::$app->request->post('user_id')))
            return 'EMAIL_IS_NOT_UNIQUE';

        if (!filter_var($inputEmail, FILTER_VALIDATE_EMAIL)) {
            return 'INCORRECT_EMAIL_FORMAT';
        }

        if ($file) {
            $imageUpload = new ImageUpload();
            $filename = $imageUpload->uploadFile($file, null); //загрузка картинки и в $filename = "название"
            $user->saveImage($filename); // сохраняем это название
        }

        $user->name = Yii::$app->request->post('name');
        $user->email = Yii::$app->request->post('email');
        $user->save();

        $data = [
            'id'=>$user->id,
            'name'=>$user->name,
            'email'=>$user->email,
            'photo'=>$user->photo,
        ];


        return json_encode($data);

    }

    public function testMailer($token, $email) {
        $url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]/site/confirm/$token";
        Yii::$app->mailer->compose()
            ->setFrom('test-dunice@yandex.ru')
            ->setTo($email)
            ->setSubject('confirmation')
            ->setTextBody($url)
            ->setHtmlBody("<b>$url</b>")
            ->send();
    }

    public function actionSocial($obj = null)
    {
        header('Access-Control-Allow-Origin: *');
//        return json_encode(Yii::$app->request->post());

        $userModel = new User();
        if (is_null($obj)) {
            $currentUser = $userModel->findByEmail(Yii::$app->request->post('email'));
        } else {
            $currentUser = $userModel->findByEmail($obj['email']);
        }

        if (!$currentUser) {

            $model = new SignupForm();
            $userData = Array();

            if (is_null($obj)) {
                $userData = [
                    'name' => Yii::$app->request->post('name'),
                    'email' => Yii::$app->request->post('email'),
                    'password' => Yii::$app->request->post('name'),
                    'confirm_password' => Yii::$app->request->post('name'),
                ];
                $user = [
                    'SignupForm' => $userData,
                    'login-button' => ''
                ];
                $model->load($user);

                if ($model->signup()) {
                    $email = Yii::$app->request->post('email');
                    $this->signupEmail($email);

                    $userModel = new User();
                    $currentUser = $userModel->findByEmail(Yii::$app->request->post('email'));

                    $currentUser->saveImage(Yii::$app->request->post('photo'));


                    $data = ArrayHelper::toArray($currentUser, [
                            'app\models\User' => [
                                'id',
                                'name',
                                'email',
                                'photo',
                            ]
                        ]
                    );

                    return json_encode($data);
                }
            } else {
                $user = new User();

                $user->name = $obj['name'];
                $user->email = $obj['email'];
                $user->password = '';
                $user->photo = $obj['photo'];
                $user->save();

                return json_encode($obj);
            }



        } else if($obj){
            $currentUser->photo = $obj['photo'];
            $currentUser->save();
            $data = ArrayHelper::toArray($currentUser, [
                    'app\models\User' => [
                        'id',
                        'name',
                        'email',
                        'photo',
                    ]
                ]
            );

            return json_encode($data);
        }

        else {
            $currentUser->photo = Yii::$app->request->post('photo');

            $currentUser->save();
            $data = ArrayHelper::toArray($currentUser, [
                    'app\models\User' => [
                        'id',
                        'name',
                        'email',
                        'photo',
                    ]
                ]
            );

            return json_encode($data);

        }
        return json_encode('Error');
    }

    public function actionSocialvk()
    {
        header('Access-Control-Allow-Origin: *');
        $objectResponse = json_decode(file_get_contents(Yii::$app->request->post('url')), true);

        $id = $objectResponse['user_id'];
        $t = $objectResponse['access_token'];
        $appID = Yii::$app->request->post('appID');
        $clientSecret = Yii::$app->request->post('clientSecret');

        $request = "https://api.vk.com/method/users.get?access_token=$t&client_id=$appID&client_secret=$clientSecret&uids=$id&fields=photo_200,status&v=5.78";
        $response = file_get_contents($request);
        $body = json_decode($response);

        $avatar = $body->response[0]->photo_200;
        $firstName = $body->response[0]->first_name;
        $lastName = $body->response[0]->last_name;
        $userName = $firstName . ' ' . $lastName;
        $email = $objectResponse['email'];

        $obj = [
            'photo' => $avatar,
            'name' => $userName,
            'email' => $email,
            'password' => $userName,
            'confirm_password' => $userName,
        ];

        $authResult = $this->actionSocial($obj);

        return $authResult;

    }

}
