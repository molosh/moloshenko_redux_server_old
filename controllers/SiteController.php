<?php

namespace app\controllers;

use app\models\Article;
use app\models\ImageUpload;
use app\models\Category;
use app\models\CreateForm;
use app\models\LoginForm;
use app\models\SendEmailForm;
use app\models\Subscription;
use app\models\Tag;
use app\models\User;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


class SiteController extends Controller
{
    /**
     * @param $action
     * {@inheritdoc}
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        header('Access-Control-Allow-Origin: *');
        $query = Article::find()->all();
//        $categories = Category::getAll();
        $data = ArrayHelper::toArray($query, [
            'app\models\Article' => [
                'id',
                'title',
                'content',
                'tags',
                'image',
                'date',
                'userName' => function ($post) {
                    $user = new User();
                    return $user->findIdentity($post->user_id)->name;
                }
            ],
        ]);
        return json_encode($data);


//        $data = Article::getAll();
//        $popular = Article::getPopular();
//        $recent = Article::getRecent();
//        $categories = Category::getAll();
//
//        return $this->renderPartial('index', [
//            'articles' => $data['articles'],
//            'pagination' => $data['pagination'],
//            'popular' => $popular,
//            'recent' => $recent,
//            'categories' => $categories,
//        ]);
    }

    public function actionView($id)
    {
        $article = Article::findOne($id);
        $popular = Article::getPopular();
        $recent = Article::getRecent();
        $categories = Category::getAll();

        $article->viewedCounter();

        return $this->render('single', [
            'article' => $article,
            'popular' => $popular,
            'recent' => $recent,
            'categories' => $categories,
        ]);
    }


    public function actionCategory($id)
    {

        $data = Category::getArticlesByCategory($id);

        $popular = Article::getPopular();
        $recent = Article::getRecent();
        $categories = Category::getAll();


        return $this->render('category', [
            'articles' => $data['articles'],
            'pagination' => $data['pagination'],
            'popular' => $popular,
            'recent' => $recent,
            'categories' => $categories,
        ]);
    }


    public function actionNew_article()
    {
        header('Access-Control-Allow-Origin: *');
        $inputTitle = Yii::$app->request->post('title');
        $inputTags = Yii::$app->request->post('tags');
        $inputContent = Yii::$app->request->post('content');


        if (!$inputTitle || !strlen($inputTags) || !$inputContent)
            return 'SOME_FIELD_IS_EMPTY';

        $article = [
            'Article' => [
                'title' =>  Yii::$app->request->post('title'),
                'description' => Yii::$app->request->post('content'),
                'content' => Yii::$app->request->post('content'),
            ],
            'category' => 1,
        ];
        $new = new Article();
        $tags = ArrayHelper::map(Tag::find()->all(), 'id', 'title'); //массив тегов из базы
        $categories = ArrayHelper::map(Category::find()->all(), 'id', 'title'); //массив категорий из базы

        if ($new->load($article) && $new->saveArticle((int)Yii::$app->request->post('user_id'))) {
            // если статья сохранена в базе (поля: заголовок, описание, контент и дата,
            // присвоен id новой статьи в базе, приступаем теперь к сохранению тегов и категории в базу.

//            $tags = Yii::$app->request->post('tags'); // массив выбранных тегов пользователем в форме
            $new->saveTags(Yii::$app->request->post('tags')); // сохраняем в таблицу article_tag выбранные теги


            $category = Yii::$app->request->post('category'); // выбранная категория пользователем в форме
            $new->saveCategory($category); // привязываем category_id к статье

            $file = UploadedFile::getInstanceByName( 'image'); //получили картинку (объект)
            if ($file) {
                $imageUpload = new ImageUpload();
                $filename = $imageUpload->uploadFile($file, null); //загрузка картинки и в $filename = "название"
                $new->saveImage($filename); // сохраняем это название
            }

            return json_encode('Success');
        }

        return $this->render('article/new', [
            'new' => $new,
            'tags' => $tags,
            'categories' => $categories,

        ]);

    }

    public function actionSearch()
    {
        $popular = Article::getPopular();
        $recent = Article::getRecent();
        $categories = Category::getAll();


        $q = Yii::$app->request->get('q');
        $q = trim($q);
        $query = Article::find()->where(['like', 'title', $q]);
        $pagination = new Pagination(['totalCount' => $query->count(), 'pageSize' => 5]);
        $articles = $query->offset($pagination->offset)->limit($pagination->limit)->all();
        return $this->render('search', compact('articles', 'pagination', 'q', 'popular', 'recent', 'categories'));
    }

    public function actionConfirm()
    {
        $token = Yii::$app->request->get('token');
        $user = User::find()->where(['email_confirm_token' => $token])->one();
        if ($user) {
            $user->status = 1;
            $user->save();
        }
        return $this->redirect('/site/index');
    }

    public function actionUser()
    {
        header('Access-Control-Allow-Origin: *');

        $user = User::findByUsername(Yii::$app->request->post('name'));

        $arr = [
                'id' => $user['id'],
                'name' => $user['name'],
                'email' => $user['email'],
                'photo' => $user['photo']
        ];
        return json_encode($arr);
    }


}
