<?php
use yii\widgets\ActiveForm;
use \yii\helpers\Html;
/* @var $new app\models\Article */
?>



<div style="margin: auto;
     width: 65%;
     justify-content: center;
     margin-bottom: 20px;">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 7]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 7]) ?>

    <label class="control-label" for="article-description">Tags(you can choose several tags)</label>
    <?= Html::dropDownList('tags',  0, $tags, ['class' => 'form-control', 'multiple' => true]) ?>
    <br>

    <label class="control-label" for="article-description">Category</label>
    <?= Html::dropDownList('category',  0, $categories,  ['class' => 'form-control']) ?>
    <br>


    <?= $form->field($model, 'image')->fileInput(['maxlength' => true]) ?>
    <br>
    <?= Html::submitButton('save', ['class' => 'btn btn-success']); ?>

    <?php $form = ActiveForm::end(); ?>

</div>