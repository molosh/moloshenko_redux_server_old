<div class="admin-default-index">
    <h1>Admin Panel</h1>
    <p>
        Hello, Admin!
        You can create/update/delete articles, tags and categories on this page.
        Also you can open <a href="/phpmyadmin", target="_blank">database</a>.
    </p>

    <br>
    <br>
    <br>
    <p>
        You may customize this page by editing the following file:<br>
        <code><?= __FILE__ ?></code>
    </p>
</div>
