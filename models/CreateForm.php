<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class CreateForm extends Model
{
    public $title;
    public $description;
    public $content;
    public $date;
    public $category;
    public $tag;
    public $image;


    public function rules()
    {
        return [
            // тут определяются правила валидации
            [['title'], 'required'],
        ];
    }

//    public function upload()
//    {
//        if ($this->validate()) {
//            $this->image->saveAs('uploads/' . $this->image->baseName . '.' . $this->image->extension);
//            return true;
//        } else {
//            return false;
//        }
//    }
}