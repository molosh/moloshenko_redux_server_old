<?php

namespace app\models;

use Yii;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
//    public $id;
//    public $username;
//    public $password;
//    public $authKey;
//    public $accessToken;
    const STATUS_DELETED = 0;
    const STATUS_NOT_ACTIVE = 1;
    const STATUS_ACTIVE = 10;

    /**
     * This is the model class for table "user".
     *
     * @property integer $id
     * @property string $name
     * @property string $email
     * @property string $password
     * @property integer $isAdmin
     * @property string $photo
     *
     * @property Comment[] $comments
     */

    public function rules()
    {
        return [
            [['isAdmin'], 'integer'],
            [['name', 'email', 'password', 'photo'], 'string', 'max' => 255],
        ];
    }

    public function saveImage($filename)
    {
        $this->photo = $filename;

        return $this->save(false);
    }

    public function getImage()
    {
//        var_dump($this->image);die;
        return ($this->photo) ? '/uploads/' . $this->photo : '../no-image.png';
    }

    public function deleteImage()
    {
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->photo);
    }




    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'isAdmin' => 'Is Admin',
            'photo' => 'Photo',
        ];
    }

    public static function findIdentity($id)
    {
        return User::findOne($id);
    }


    public function getId()
    {
        return $this->id;
    }


    public function getAuthKey()
    {

    }


    public function validateAuthKey($authKey)
    {

    }

    public static function findIdentityByAccessToken($token, $type = null)
    {

    }




    public static function findOthersByUsername($username, $id)
    {
        return User::find()->where(['=', 'name', $username])->andWhere(['<>', 'id', $id])->one();
    }

    public static function findOthersByEmail($email, $id)
    {
        return User::find()->where(['=', 'name', $email])->andWhere(['<>', 'id', $id])->one();
    }




    public static function findByUsername($username)
    {
        return User::find()->where(['name' => $username])->one();
    }

    public static function findByEmail($email)
    {
        return User::find()->where(['email' => $email])->one();
    }





    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->getAttribute('password'));
    }

    public function create()
    {
        return $this->save(false);
    }




    public function sendMail($view, $subject, $params = []) {
        // Set layout params
        \Yii::$app->mailer->getView()->params['name'] = $this->name;

        $result = \Yii::$app->mailer->compose([
            'html' => 'views/' . $view . '-html',
            'text' => 'views/' . $view . '-text',
        ], $params)->setTo([$this->email => $this->name])
            ->setSubject($subject)
            ->send();

        // Reset layout params
        \Yii::$app->mailer->getView()->params['name'] = null;

        return $result;
    }









    public function generateSecretKey()
    {
        $this->secret_key = Yii::$app->security->generateRandomString().'_'.time();
    }
    public function removeSecretKey()
    {
        $this->secret_key = null;
    }
    public static function isSecretKeyExpire($key)
    {
        if (empty($key))
        {
            return false;
        }
        $expire = Yii::$app->params['secretKeyExpire'];
        $parts = explode('_', $key);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }






}
